Readme
------
Tagcloud is a small module forked from Tagadelic, without any databases, or configuration, that generates pages with weighted tags.
Tagcloud is an out of the box, ready to use module, if you want simple weighted tag clouds. With, or without some small CSS moderations this will probably suit most cases. 
Unlike Tagadelic, TagCloud does not claim to be an API.
